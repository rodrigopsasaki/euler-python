from utilities.primes import get_prime_factors


def problem3():
    return max(get_prime_factors(600851475143))
