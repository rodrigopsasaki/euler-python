
def problem6():
    numbers = range(1, 101)
    sum_of_numbers = sum(numbers)
    sum_of_squares = sum(map(lambda n: n * n, numbers))
    square_of_sums = sum_of_numbers * sum_of_numbers
    return square_of_sums - sum_of_squares
