from utilities.data_importer import get_data
from operator import mul


def problem8():
    number_string = get_data('problem8_data').read()
    numbers = [int(x) for x in number_string]
    return max([reduce(mul, numbers[i:i+13]) for i in range(len(numbers) - 13)])