def is_palindrome(number):
    number_string = str(number)
    if not number_string:
        return True
    elif number_string[0] == number_string[-1]:
        return is_palindrome(number_string[1:-1])
    else:
        return False


def problem4():

    result = 0
    for x in range(999, 0, -1):
        for y in range(999, 0, -1):
            product = x * y
            if is_palindrome(product):
                if product > result:
                    result = product

    return result
