def euclid_gcd(a, b):
    while b:
        a, b = b, a % b
    return a


def lcm(x, y):
    return x * y / euclid_gcd(x, y)


def list_lcm(numbers):
    return internal_list_lcm(1, numbers)


def internal_list_lcm(acc, numbers):
    print acc
    if not numbers:
        return acc
    else:
        head, tail = numbers[0], numbers[1:]
        return internal_list_lcm(lcm(acc, head), tail)


def problem5():
    return list_lcm(range(1, 21))