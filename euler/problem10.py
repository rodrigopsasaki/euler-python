from utilities.primes import get_primes


def problem10():
    primes = get_primes(1)
    total_sum = 0
    for prime in primes:
        if prime > 2000000:
            return total_sum
        total_sum += prime
