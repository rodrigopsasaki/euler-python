def problem9():
    total_sum = 1000

    for b in range(total_sum / 2):
        for a in range(1, b):
            c = total_sum - a - b
            if a * a + b * b == c * c:
                return a * b * c