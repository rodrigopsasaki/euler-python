from utilities.sequences import fibonacci


def problem2():
    result = 0
    for term in fibonacci():
        if term > 4000000:
            break
        if term % 2 == 0:
            result += term

    return result
