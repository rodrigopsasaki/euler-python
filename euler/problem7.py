from utilities.primes import get_primes


def problem7():
    primes = get_primes(1)
    for i, prime in enumerate(primes):
        if i == 10000:
            return prime
