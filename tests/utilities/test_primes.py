from utilities import primes


def test_5_is_prime():
    assert primes.is_prime(5) == True


def test_4_is_not_prime():
    assert primes.is_prime(4) == False


def test_0_is_not_prime():
    assert primes.is_prime(0) == False


def test_negative_is_not_prime():
    assert primes.is_prime(-1) == False


def test_gets_correct_next_prime():
    primes_generator = primes.get_primes(1)
    assert next(primes_generator) == 2
    assert next(primes_generator) == 3
    assert next(primes_generator) == 5
    assert next(primes_generator) == 7


def test_3_and_5_are_prime_factors_of_15():
    factors = primes.get_prime_factors(15)
    assert factors == [3, 5]
