from utilities import sequences


def test_correct_first_10_fibonacci_terms():
    fibo = sequences.fibonacci()
    assert next(fibo) == 0
    assert next(fibo) == 1
    assert next(fibo) == 1
    assert next(fibo) == 2
    assert next(fibo) == 3
    assert next(fibo) == 5
    assert next(fibo) == 8
    assert next(fibo) == 13
    assert next(fibo) == 21
    assert next(fibo) == 34
    assert next(fibo) == 55
    assert next(fibo) == 89


