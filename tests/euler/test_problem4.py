from euler import problem4


def test_101_is_palindrome():
    assert problem4.is_palindrome(101) == True


def test_40404_is_palindrome():
    assert problem4.is_palindrome(40404) == True


def test_100_is_not_palindrome():
    assert problem4.is_palindrome(100) == False


def test_problem4():
    assert problem4.problem4() == 906609
