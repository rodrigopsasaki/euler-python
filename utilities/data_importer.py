import os


def get_data(file):
    current_dir = os.path.dirname(__file__)
    data_file = os.path.join(os.path.dirname(os.path.join(current_dir)), 'data', file)
    return open(data_file)
