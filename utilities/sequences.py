def fibonacci():
    yield 0
    a, b = 0, 1

    while True:
        yield b
        b += a
        a = b - a
